/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 * Hacked (C) 1996 Tristan Tarrant
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "../include/sane/config.h"

#include <stdlib.h>
#include <string.h>
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#include <gtk/gtk.h>
#include "progress.h"

void
progress_cancel (GtkWidget * widget, gpointer data)
{
  Progress_t *p = (Progress_t *) data;

  (*p->callback) ();
}


Progress_t *
progress_new (char *title, char *text, GtkWindow * parent,
	      GtkSignalFunc callback, gpointer callback_data)
{
  GtkWidget *button, *label;
  GtkBox *vbox, *hbox;
  Progress_t *p;
#if GTK_MAJOR_VERSION < 2
  gint x, y;
#endif

  p = (Progress_t *) malloc (sizeof (Progress_t));
  p->callback = callback;
  p->firstTime = 0;

  p->shell = gtk_dialog_new ();
#if GTK_MAJOR_VERSION < 2
  gtk_window_get_position (GTK_WINDOW (parent), &x, &y);
  gtk_widget_set_uposition (p->shell, x, y);
#else
  gtk_window_set_transient_for (GTK_WINDOW (p->shell), parent);
#endif
  gtk_window_set_title (GTK_WINDOW (p->shell), title);
  vbox = GTK_BOX (GTK_DIALOG (p->shell)->vbox);
  hbox = GTK_BOX (GTK_DIALOG (p->shell)->action_area);

  gtk_container_border_width (GTK_CONTAINER (vbox), 7);

  label = gtk_label_new (text);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_box_pack_start (vbox, label, FALSE, TRUE, 0);

  p->pbar = gtk_progress_bar_new ();
  gtk_widget_set_usize (p->pbar, 200, 20);
  gtk_box_pack_start (vbox, p->pbar, TRUE, TRUE, 0);

#ifdef HAVE_SYS_TIME_H
  p->etaIndicator = gtk_label_new (ETA_LABEL ": ?");
  gtk_misc_set_alignment (GTK_MISC (p->etaIndicator), 0.0, 0.5);
  gtk_box_pack_start (vbox, p->etaIndicator, FALSE, TRUE, 0);
#endif

  /* no cancel button if no callback */
  if (callback != NULL)
    {
      button = gtk_toggle_button_new_with_label ("Cancel");
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
			  (GtkSignalFunc) progress_cancel, p);
      gtk_box_pack_start (hbox, button, TRUE, TRUE, 0);
    }

  gtk_widget_show (label);
  gtk_widget_show (p->pbar);
#ifdef HAVE_SYS_TIME_H
  gtk_widget_show (p->etaIndicator);
#endif
  if (callback != NULL)
    gtk_widget_show (button);
  gtk_widget_show (GTK_WIDGET (p->shell));
  gtk_progress_bar_update (GTK_PROGRESS_BAR (p->pbar), 0);
  return p;
}

void
progress_free (Progress_t * p)
{
  if (p)
    {
      gtk_widget_destroy (p->shell);
      free (p);
    }
}

void
progress_update (Progress_t * p, gfloat newval)
{
#ifdef HAVE_SYS_TIME_H
  struct timeval tv;
  int now;
  char buff[40];
  int remaining;
#endif

  if (!p)
    {
      return;
    }

  gtk_progress_bar_update (GTK_PROGRESS_BAR (p->pbar), newval);

#ifdef HAVE_SYS_TIME_H
  gettimeofday (&tv, NULL);
  now = tv.tv_sec * 1000 + tv.tv_usec / 1000;

  if (p->firstTime == 0)
    {
      p->lastTime = p->firstTime = now;
      p->firstVal = newval;
      return;
    }

  if (newval > p->firstVal && now - p->lastTime > 1000)
    {
      remaining =
	(int) ((now - p->firstTime) * (1.0 - newval) / (newval -
							p->firstVal) /
	       1000.0 + 0.9);

      p->lastTime = now;

      if (remaining >= 3600)
	{
	  snprintf (buff, sizeof (buff), ETA_LABEL ": %d h %d min",
		    remaining / 3600, (remaining / 60) % 60);
	}
      else if (remaining >= 600)
	{
	  snprintf (buff, sizeof (buff), ETA_LABEL ": %d min",
		    remaining / 60);
	}
      else if (remaining >= 60)
	{
	  snprintf (buff, sizeof (buff), ETA_LABEL ": %d min %d sec",
		    remaining / 60, remaining % 60);
	}
      else
	{
	  snprintf (buff, sizeof (buff), ETA_LABEL ": %d sec", remaining);
	}

      gtk_label_set_text (GTK_LABEL (p->etaIndicator), buff);
    }
#endif
}
